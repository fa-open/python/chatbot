# 读取配置文件
# coding=utf-8

from configparser import SafeConfigParser


def get_config(config_file='seq2seq.ini'):
    """
    读取本地配置文件
    :param config_file:
    :return:
    """
    parser = SafeConfigParser()
    parser.read(config_file, encoding="utf-8")
    # get the ints, floats and strings
    _conf_ints = [(key, int(value)) for key, value in parser.items('ints')]
    # _conf_floats = [(key, int(value) for key, value in parser.items('floats'))]
    _conf_strings = [(key, str(value)) for key, value in parser.items('strings')]
    return dict(_conf_ints + _conf_strings)


# if __name__ == '__main__':
#     dict = get_config()
#     pass

