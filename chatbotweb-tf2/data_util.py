# coding = utf-8
import os
import jieba
import get_config
from zhon.hanzi import punctuation
import re
# 结巴是国内的一个分词python库，分词效果非常不错

gConfig = get_config.get_config() # 读取配置文件
conv_path = gConfig['resource_data']

if not os.path.exists(conv_path):
    exit()

# 将训练集的数据识别读取并存入一个List中，大概分为以下几个步骤
# 1. 打开文件
# 2. 读取文件中的内容，并对文件的数据进行初步处理
# 3. 找出我们想要的数据存储下来
convs = []  # 用于存储对话的列表
with open(conv_path, encoding="utf-8") as f:
    one_conv = []  # 存储一次完整对话
    for line in f:
        line = line.strip('\n').replace('?', '')    # 去除换行符，并将原文件中已经分词的标记去掉，重新用结巴分词
        line = re.sub(r"[%s]+" % punctuation, "", line)
        if line == '':
            continue
        if line[0] == gConfig['e']:
            if one_conv:
                convs.append(one_conv)
            one_conv = []
        elif line[0] == gConfig['m']:
            one_conv.append(line.split(' ')[1])  # 将一次完整的对话存储下来

# 接下来，我们需要对训练集的对话进行分类，分为问和答，或者叫上文、下文，这个主要是作为encoder和decoder的训练数据
# 我们一般分为一下几个步骤
# 1. 初始化变量，ask response为list
# 2. 按照语句的顺序来分为问句和答句，根据行数的奇偶性来判断
# 3. 在存储语句的时候对语句使用结巴分词，jieba.cut

# 把对话分为问与答两个部分
seq = []


# if __name__ == '__main__':
#     ret = jieba.cut("我是中国人")
#     for r in ret:
#         print(r)

